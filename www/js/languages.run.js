angular
  .module('ariaRent')
  .config(["$translateProvider", function($translateProvider){
    $translateProvider.preferredLanguage('en');
    $translateProvider.fallbackLanguage("en");
    $translateProvider.useStaticFilesLoader({
      'prefix': 'languages/',
      'suffix': '.json'
    });
  }]);
