angular
  .module('ariaRent', ['ionic', 'pascalprecht.translate','uiGmapgoogle-maps', 'ngCordova', 'ngStorage'])
  .config(["$stateProvider", "$urlRouterProvider",
      function($stateProvider, $urlRouterProvider) {
          $stateProvider
            .state('app', {
              url: '/app',
              templateUrl: 'views/app/app.html',
              controller: 'AppCtrl',
              abstract: true
            })
            .state('app.reality', {
              url: '/home',
              templateUrl: 'views/reality/reality.html',
              controller: 'RealityCtrl',
              resolve: {
                properties: ["PropertyService", function (PropertyService) {
                  return PropertyService.getElements();
                }]
              }
            })
            .state('app.searchSale', {
              url: '/search',
              templateUrl: 'views/search/searchSale.html',
              controller: 'SearchCtrl'
            })
            .state('app.searchRental', {
              url: '/search',
              templateUrl: 'views/search/searchRental.html',
              controller: 'SearchCtrl'
            })
            .state('app.property', {
              url: '/property',
              templateUrl: 'views/reality/property.html',
              controller: 'PropertyCtrl'
            })
            .state('app.auth', {
                url: '/auth',
                templateUrl: 'views/auth/auth.html'
            })
            .state('app.auth.signIn', {
                url: '/signIn',
                templateUrl: 'views/auth/signIn.html',
                controller: 'SignInCtrl'
            })
            .state('app.auth.signUp', {
                url: '/signUp',
                templateUrl: 'views/auth/signUp.html',
                controller: 'SignUpCtrl'
            })
            .state('app.auth.resetPassword', {
                url: '/resetPassword',
                templateUrl: 'views/auth/resetPassword.html',
                controller: 'ResetPasswordCtrl'
            })
            .state('app.auth.codeConfirm', {
              url: '/codeConfirm',
              templateUrl: 'views/auth/codeConfirm.html',
              controller: 'CodeConfirmCtrl'
            })
            .state('app.auth.createPassword', {
              url: '/createPassword',
              templateUrl: 'views/auth/createPassword.html',
              controller: 'CreatePasswordCtrl'
            })
            .state('app.auth.settings', {
              url: '/settings',
              templateUrl: 'views/auth/settings.html',
              controller: 'SettingsCtrl'
            })
            .state('app.likeList', {
              url: '/likeList',
              templateUrl: 'views/likeList/likeList.html',
              controller: 'LikeListCtrl',
              resolve: {
                properties: ["LikeService", "$localStorage", function (LikeService, $localStorage) {
                  return LikeService.getLikedPropertied();
                }]
              }
            })
            .state('app.manageList', {
              url: '/manageList',
              templateUrl: 'views/manageList/manageList.html',
              controller: 'ManageListCtrl'
            })
            .state('app.add', {
              url: '/add',
              templateUrl: 'views/manageList/addEdit.html',
              controller: 'AddEditCtrl'
            })
            .state('app.edit', {
              url: '/edit',
              templateUrl: 'views/manageList/addEdit.html',
              controller: 'AddEditCtrl'
            });

          $urlRouterProvider.otherwise('/app/home');
      }
  ])
  .constant("networkConfig", {
    //"url": "http://localhost:3000",
    "url": "https://aria-rent.herokuapp.com",
    "baseUrl": "/api/v1",
    "facebookAppId": "882243581831101"
  })
  .factory('LoaderService', [function () {
    return {
      showLoader: function(){

      },
      hideLoader: function(){

      },
      showServerErrors: function(result){
        alert(result.data.errors[0].message);
      }
    }
  }])
  .run(["$ionicPlatform", "UserService",
      function($ionicPlatform, UserService) {
        UserService.startupAuth().then(function(){
          $ionicPlatform.ready(function() {
            if (window.cordova && window.cordova.plugins.Keyboard) {
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
              StatusBar.styleDefault();
              $cordovaStatusbar.overlaysWebView(true)
              $cordovaStatusBar.style(1);
            }
          });
        });
      }
  ])
  .factory('network', ["$http", "networkConfig", '$localStorage', function($http, networkConfig, $localStorage){

    function baseUrl(x){
      return networkConfig.url + networkConfig.baseUrl + x;
    }

    function request(data){
      var headers = {};
      if($localStorage.token){
        headers.authtoken = $localStorage.token;
      }
      return $http({
        url: baseUrl(data.url),
        method: data.method,
        headers: headers,
        data: data.data
      });
    }
    return {
      post: function(url, data) {
        return request({
          url: url,
          method: 'POST',
          data: data
        })
      },
      get: function(url){
        return request({
          url: url,
          method: 'GET'
        })
      },
      delete: function(url, data){
        return request({
          url: url,
          method: 'DELETE',
          data: data
        })
      }
    }
  }])
  .run(["$ionicPlatform", "$rootScope", function($ionicPlatform, $rootScope) {
      $ionicPlatform.ready(function() {
          if(window.cordova && window.cordova.plugins.Keyboard) {
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          }
          if(window.StatusBar) {
              StatusBar.styleDefault();
          }


          window.addEventListener('deviceorientation', function(eventData) {

            var yTilt = eventData.alpha;

            var xTilt = eventData.gamma;


            var backgroundPositionValue = 'translateX(' + ((xTilt*50/180) | 0) + 'px) ';
            backgroundPositionValue += 'translateY(' + ((yTilt*50/180) | 0) + 'px) ';

            Array.prototype.slice.call(document.getElementsByClassName('background'), 0).forEach(function(background){
              background.style.transform = backgroundPositionValue;
            });
          }, false);
      });
  }]);
