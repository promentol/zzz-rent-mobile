(function (angular) {
  angular
    .module('ariaRent')
    .controller('ManageListCtrl', ["$scope", ManageListCtrl]);

  function ManageListCtrl($scope){
    $scope.properties = [{
      "id":159,"latitude":40.1809,"longitude":44.5083,"price":488,"currency":"usd","buildingType":"stone","buildingYear":null,"community":null,"street_am":"dsfd","street_ru":null,"street_en":"dfsd","area":80,"bathroom":null,"room":2,"floor":2,"elevator":null,"balcony":null,"cellar":null,"loft":null,"garage":null,"quality":5,"ceiling":null,"flooring":null,"internet":true,"cableTV":null,"furniture":true,"entrance":null,"parking":"0","amenity":null,"height":null,"isPrivate":null,"description":null,"zip":null,"medias":[],"like":null,"shareLink":"http://aria-rent.am/asd/asd"
      },{
        "id":160,"latitude":40.1752,"longitude":44.5182,"price":966,"currency":"usd","buildingType":"stone","buildingYear":null,"community":null,"street_am":null,"street_ru":null,"street_en":"sdfdsf","area":90,"bathroom":null,"room":3,"floor":5,"elevator":null,"balcony":null,"cellar":null,"loft":null,"garage":null,"quality":5,"ceiling":null,"flooring":null,"internet":null,"cableTV":null,"furniture":true,"entrance":null,"parking":"0","amenity":null,"height":null,"isPrivate":null,"description":null,"zip":null,"medias":[],"like":null,"shareLink":"http://aria-rent.am/asd/asd"
        },{
          "id":161,"latitude":40.183,"longitude":44.5092,"price":846,"currency":"usd","buildingType":"monolit","buildingYear":null,"community":null,"street_am":null,"street_ru":null,"street_en":"dsfdsf","area":100,"bathroom":null,"room":3,"floor":9,"elevator":null,"balcony":null,"cellar":null,"loft":null,"garage":null,"quality":5,"ceiling":null,"flooring":null,"internet":true,"cableTV":null,"furniture":true,"entrance":null,"parking":"0","amenity":null,"height":null,"isPrivate":null,"description":null,"zip":null,"medias":[],"like":null,"shareLink":"http://aria-rent.am/asd/asd"
       }];
  }
})(window.angular);
