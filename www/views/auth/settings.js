(function(angular){
  angular
    .module('ariaRent')
    .controller('SettingsCtrl', ["$scope", "$ionicHistory", "UserService", "LoaderService", SettingsCtrl]);

  function SettingsCtrl($scope, $ionicHistory, UserService, LoaderService){
    var user = UserService.getSettings();

    $scope.settings = {
      language: user.language,
      currency: user.currency
    };

    $scope.saveSettings = function(){
      LoaderService.showLoader();
      UserService.saveSettings($scope.settings).then(function(){
        LoaderService.showLoader();
      }).then(function(){
        LoaderService.hideLoader();
        $ionicHistory.goBack();
      }, function(){
        LoaderService.hideLoader();
      })
    };
  }
})(window.angular);
