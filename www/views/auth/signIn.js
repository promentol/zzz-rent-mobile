(function(angular){
  angular
    .module('ariaRent')
    .controller('SignInCtrl', ["$scope", "$ionicHistory", "UserService", "LoaderService", SignInCtrl]);

  function SignInCtrl($scope, $ionicHistory, UserService, LoaderService) {

    $scope.data = {};
    $scope.skip = function(){
      $ionicHistory.goBack();
    };
    $scope.signByFacebook = function(){
      LoaderService.showLoader();
      UserService.signByFacebook().then(function(){
        $ionicHistory.goBack();
        LoaderService.hideLoader();
      }, function(){
        LoaderService.hideLoader();
      });
    };
    $scope.signInByEmail = function(){
      LoaderService.showLoader();
      UserService.signInByEmail($scope.data).then(function(){
        $ionicHistory.goBack();
        LoaderService.hideLoader();
      }, function(result){
        LoaderService.hideLoader();
        LoaderService.showServerErrors(result);
      });
    };
  }
})(window.angular);
