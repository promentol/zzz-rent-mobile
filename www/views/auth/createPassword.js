(function (angular) {
  angular
    .module('ariaRent')
    .controller('CreatePasswordCtrl', ["UserService", "$stateParams", "$ionicHistory", "$scope", "LoaderService", CreatePasswordCtrl]);

  function CreatePasswordCtrl(UserService, $stateParams, $ionicHistory, $scope, LoaderService){
    $scope.data = {};
    $scope.createPassword = function(){
      console.log($stateParams.code);
      console.log($stateParams);
      LoaderService.showLoader();
      UserService.createPassword($scope.data, $stateParams.code).then(function(){
        LoaderService.hideLoader();
        $ionicHistory.goBack(-3);
      }, function(result){
        LoaderService.hideLoader();
        LoaderService.showServerErrors(result);
      })
    }

  }
})(window.angular);
