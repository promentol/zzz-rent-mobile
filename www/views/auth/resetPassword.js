(function (angular) {
  angular
    .module('ariaRent')
    .controller('ResetPasswordCtrl', ["UserService", "$state", "$scope", "LoaderService", ResetPasswordCtrl]);

  function ResetPasswordCtrl(UserService, $state, $scope, LoaderService){
    $scope.data = {};
    $scope.sendRequest = function(){
      LoaderService.showLoader();
      UserService.sendPasswordRecoverRequest($scope.data.email).then(function(){
        LoaderService.hideLoader();
        $state.go('app.auth.codeConfirm');
      }, function(result){
        LoaderService.hideLoader();
        LoaderService.showServerErrors(result);
      })
    }

  }
})(window.angular);
