(function(angular){
  angular
    .module('ariaRent')
    .controller('CodeConfirmCtrl', ["UserService", "$state", "$scope", "LoaderService", CodeConfirmCtrl]);

  function CodeConfirmCtrl(UserService, $state, $scope, LoaderService){
    $scope.data = {};
    $scope.confirmCode = function() {
      LoaderService.showLoader();
      UserService.verifyToken($scope.data.code).then(function () {
        LoaderService.hideLoader();
        console.log($scope.data.code);
        $state.go('app.auth.createPassword', {
          code: $scope.data.code
        });
      }, function (result) {
        LoaderService.hideLoader();
        LoaderService.showServerErrors(result);
      })
    }
  }
})(window.angular);
