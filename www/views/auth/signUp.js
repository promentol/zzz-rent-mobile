(function(angular){
  angular
    .module('ariaRent')
    .controller('SignUpCtrl', ["$scope", "UserService", "LoaderService", "$ionicHistory", SignUpCtrl]);

  function SignUpCtrl($scope, UserService, LoaderService, $ionicHistory) {
    $scope.data = {};
    $scope.signUp = function(){
      LoaderService.showLoader();
      UserService.signUpByEmail($scope.data).then(function(){
        LoaderService.hideLoader();
        $ionicHistory.goBack(-2);
      }, function (result) {
        LoaderService.hideLoader();
        LoaderService.showServerErrors(result);
      });
    }
  }

})(window.angular);
