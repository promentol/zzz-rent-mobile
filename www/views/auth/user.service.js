(function(angular){
  angular
    .module('ariaRent')
    .factory('UserService', ["$q","$cordovaOauth", "$localStorage", "networkConfig", 'network', '$translate', '$rootScope', UserService]);

  function UserService($q, $cordovaOauth, $localStorage, networkConfig, network, $translate, $rootScope){
    var exports = {};
    exports.createWithEmail = function(data){
    };

    exports.signByFacebook = function(){
      return $cordovaOauth.facebook(networkConfig.facebookAppId, ["email"]).then(function(result){
        return result.access_token;
      }).then(function (token) {
        return internals.signIn({
          language: $localStorage.settings.language,
          currency: $localStorage.settings.currency,
          oauth_token: token,
          provider: 'facebook'
        })
      });
    };

    exports.signUpByEmail = function(data){
      return  network.post('/auth/user', {
        email: data.email,
        password: data.password,
        confirm: data.confirm,
        name: data.name,
        language: $localStorage.settings.language,
        currency: $localStorage.settings.currency
      }).then(function (result) {
        return internals.setAuth(result.data);
      });
    };

    exports.sendPasswordRecoverRequest = function(email){
      return network.post('/passwordRecovery', {
        email: email
      })
    };
    exports.verifyToken = function(code){
      return network.post('/passwordRecovery/verify', {
        code: code
      })
    };

    exports.signInByEmail = function(data){
      return internals.signIn({
        provider: 'email',
        email: data.email,
        password: data.password
      });
    };

    exports.createPassword = function(data, code){
      return network.post('/passwordRecovery/recover', {
        password: data.password,
        confirm: data.confirm,
        code: code
      })
    };

    exports.saveSettings = function(data, code){
      return network.put('/auth/me', {
        language: data.language,
        currency: data.currency
      })
    };

    exports.getUser = function(){
      return $localStorage.user;
    };

    exports.getSettings = function(){
      var settings = {};
      return {
        language: $localStorage.settings.language,
        currency: $localStorage.settings.currency
      }
    };

    var internals = {};

    internals.signIn = function(credentials){
      return network.post('/auth', credentials).then(function(result){
        return internals.setAuth(result.data);
      });
    };

    internals.applySettings = function (settings) {
      $translate.use(settings.language);
    };

    exports.startupAuth = function(){
      return network.get('/auth').then(function(result){
        return internals.setAuth(result.data);
      });
    };

    internals.setAuth = function(data){

      $localStorage.user = data.user;
      $localStorage.token = data.token;

      $localStorage.settings = {
        language: data.user.language,
        currency: data.user.currency
      };

      internals.applySettings($localStorage.settings);
      $rootScope.user = $localStorage.user;
      return data;
    };

    return {
      signUpByEmail: exports.signUpByEmail,
      signInByEmail: exports.signInByEmail,
      signByFacebook: exports.signByFacebook,
      sendPasswordRecoverRequest: exports.sendPasswordRecoverRequest,
      verifyToken: exports.verifyToken,
      createPassword: exports.createPassword,
      saveSettings: exports.saveSettings,
      getUser: exports.getUser,
      startupAuth: exports.startupAuth,
      getSettings: exports.getSettings
    }
  }
})(window.angular);
