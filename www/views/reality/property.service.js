(function(angular) {
  angular
    .module('ariaRent')
    .factory('PropertyService', ["$q", "network", PropertyService]);
  function PropertyService($q, network) {
    var exports = {};
    exports.getElements = function(filters){
      return network.get('/property', filters).then(function(result){
        return result.data;
      });
    };
    return {
      getElements: exports.getElements
    };
  }
})(window.angular);
