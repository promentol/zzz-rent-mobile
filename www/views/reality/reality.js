(function(angular){
  angular
    .module('ariaRent')
    .controller('RealityCtrl', ["$scope", "properties", "$state", "$rootScope", "LikeService", "$cordovaSocialSharing", RealityCtrl]);
  function RealityCtrl($scope, properties, $state, $rootScope, LikeService, $cordovaSocialSharing){

    /*

     .propImg {
     max-width: 200px;
     }
     .propImg img {
     max-width: 100%;
     }

     .custom-info {
     -webkit-transform: translate(-50%, -100%) !important;
     transform: translate(-50%, -100%) !important;
     margin-top: -35px;
     }

     .custom-info [align="right"] {
     display: none;
     }

     */

    $scope.like = function(){
      if($rootScope.user){
        if(!$scope.currentProperty.like){
          LikeService.like($scope.currentProperty.id).then(function(){
            $scope.currentProperty.like = true;
          })
        }
      }
    };
    $scope.getBackgroundStyle = function(imagepath){
      return {
        'background-image':'url(' + imagepath + ')'
      }
    };

    $scope.goTo = function(x){
      $rootScope.currentProperty = x;
      $state.go('app.property')
    };

    $rootScope.$on('likeDisabled', function(e, id){
      $scope.properties.forEach(function(x){
        if(x.id == id) x.like = false;
      })
    });

    properties.forEach(function(x){
      x.f = function(property){
        return $scope.f(property);
      }
    });
    $scope.properties = properties;
    $scope.infoWindow = {
      template: '<div class="price-marker" ng-mousedown="parameter.f(parameter)">${{parameter.price/1000}}հազ</div>',
      options: {
        boxClass: "custom-info",
        pixelOffset: new google.maps.Size(-30, -32)
      }
    };

    $scope.share = function(){
      $cordovaSocialSharing.share(null, null, null, $scope.currentProperty.shareLink).then(function(x){

      }, function(x){
        console.log(x);
      });
    };

    $scope.f = function(property){
      $scope.currentProperty = property;
    };
    $scope.map = {
      center: {
        latitude: 40.1811100,
        longitude: 44.5136100
      },
      options: {},
      zoom: 15,
      events: {
        dragend: function(x){
          console.log(x);
        }
      },
      bounds: {}
    };
  }

})(window.angular);
