(function (angular) {
  angular
    .module('ariaRent')
    .controller('LikeListCtrl', ["properties", "LikeService", "$scope", "LoaderService", LikeListCtrl]);

  function LikeListCtrl(properties, LikeService, $scope, LoaderService){
    $scope.properties = properties;
    $scope.removeLike = function(index){
      LoaderService.showLoader();
      LikeService.disLike(properties[index].id).then(function(){
        $scope.properties[index].noShow = true;
        $scope.$emit('likeDisabled', properties[index].id);
        LoaderService.hideLoader();
      });
    };
  }
})(window.angular);
