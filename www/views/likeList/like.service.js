(function(angular) {
  angular
    .module('ariaRent')
    .factory('LikeService', ["$q", "network", LikeService]);
  function LikeService($q, network) {
    var exports = {};
    exports.getLikedPropertied = function(){
      return network.get('/likeList').then(function(result){
        return result.data;
      });
    };
    exports.like = function(propertyId){
      return network.post('/likeList', {
        propertyId: propertyId
      });
    };
    exports.disLike = function(propertyId){
      return network.delete('/likeList/'+propertyId);
    };
    return {
      getLikedPropertied: exports.getLikedPropertied,
      like: exports.like,
      disLike: exports.disLike
    };
  }
})(window.angular);
